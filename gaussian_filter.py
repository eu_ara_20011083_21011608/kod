# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
import os
import cv2
import numpy as np
import copy

import dask
from dask import delayed

import cupy as cp


window_size=5
# Create a Gaussian kernel
sigma = 10 # You can adjust the sigma value as needed
gaussian_kernel = cv2.getGaussianKernel(window_size, sigma)
gaussian_kernel = gaussian_kernel * gaussian_kernel.T
 

gaus_path= '/home/selim2204/spyder/gauss result'
gaus_fark_path='/home/selim2204/spyder/gaus fark'

def get_all_frames(video_path,dataset_number):
    
    delayed_results = []
    
    frame_chunk = []
    # Create a VideoCapture object
    cap = cv2.VideoCapture(video_path)

    # Check if the video file was successfully opened
    if not cap.isOpened():
        print("Error: Could not open video.")
        return frame_chunk
    
    frame_count = 0
    chunk_size = 1
    frame_index=1
    
    iteration=1
    # Read and store all frames from the video
    
    while True and iteration<100:
        print("iteration="+str(iteration))
        # Read a frame from the video
        ret, frame = cap.read()

        # If the frame was not read successfully, break the loop
        if not ret:
            break

        # Append the frame to the list of frames
        frame_count += 1
        frame_chunk.append(frame)
        
        
        # Check if frame capacity is achieved
        if frame_count >= chunk_size:
            # Use Dask to parallelize processing of frames
            delayed_results.append(delayed(get_frame_chunk_and_filter)(frame_chunk, frame_index,dataset_number))

            
            
          
            # Reset frames list for the next batch of frames
            
            frame_chunk = []
            frame_index=frame_index+frame_count
            frame_count = 0
        iteration+=1

    
    chunked_tasks = [delayed_results[i:i+2] for i in range(0, len(delayed_results), 2)]
    dask.visualize(*chunked_tasks, filename='graph.png')
    
    
    computed_results=np.array(dask.compute(*delayed_results))
    #computed_results=dask.compute(*delayed_results)

    '''
    tmp_var = np.count_nonzero(all_frames- computed_results)
    print("test",tmp_var)
    '''
    return computed_results




def get_frame_chunk_and_filter(frame_chunk, frame_index, dataset_number):
    
    
    for i in range(len(frame_chunk)):
        
        
        
        filename = "dataset_"+str(dataset_number)+"/segment save/output_video_" +str(dataset_number)+"_original_"+str(frame_index) + ".txt"
        filename_png="output_video_" +str(dataset_number)+"_original_"+str(frame_index) + ".png"
       
        
       # Check if the file exists
        if os.path.exists(filename):
            bbox_data, vertices_data = parse_vertices(filename)
            
            frame_save_tmp=copy.deepcopy(frame_chunk[i])
            
            
            apply_filter_using_bbox(bbox_data,vertices_data,frame_chunk[i],frame_index)
            
            # Write frame_chunk[i] to gaus_path/filename_png
            output_path = os.path.join(gaus_path, filename_png)
            cv2.imwrite(output_path, frame_chunk[i])
            print("Image saved at:", output_path)  # Debug statement


            # Write frame_chunk[i] to gaus_path/filename_png
            output_path = os.path.join(gaus_fark_path, filename_png)
            cv2.imwrite(output_path, frame_save_tmp-frame_chunk[i])
            print("Image saved at:", output_path)  # Debug statement

            
            
        else:
            # File doesn't exist, handle the situation here
            print("File does not exist. Cannot parse vertices.")
            # You might want to raise an error, log a message, or handle it differently based on your requirements
    
    
       
                    
        frame_index += 1





 
    




def is_point_inside_polygon(point, vertices):
    
    threshold=15
    
    # Check if the point is inside the polygon defined by vertices
    distance = cv2.pointPolygonTest(vertices, point, True)
    return (distance < threshold and distance >= 0)


def apply_gaussian_filter(img, center, window_size):
   
    
    # Extract the region of interest centered around the point
    x, y = center
    roi = img[y - window_size//2:y + window_size//2 + 1, x - window_size//2:x + window_size//2 + 1]
    
   
    
    if roi.size == 0:
        print("Error: Empty ROI at center:", center)
        return

    # Apply the Gaussian filter
    filtered_roi = cv2.filter2D(src=roi, ddepth=-1, kernel=gaussian_kernel) 
    #filtered_roi = cv2.filter2D(roi, ddepth=-1, gaussian_kernel)
    
    # Update the original image with the filtered region
    img[y - window_size//2:y + window_size//2 + 1, x - window_size//2:x + window_size//2 + 1] = filtered_roi
    



    

def parse_vertices(filename):
    # Define a function to parse the file and extract vertices and bounding box information
    vertices_list = []  # Initialize an empty list to store vertices
    bbox_list = []  # Initialize an empty list to store bounding box coordinates

    with open(filename, 'r') as file:
        for line in file:
            # Split the line into components
            components = list(map(int, line.strip().split()))

            # Check if there are enough elements to extract vertices and bounding box
            if len(components) >= 7:  # Check for at least 7 elements (ID, bounding box, and vertices)
                # Extract bounding box coordinates
                bbox = components[1:5]
                bbox_list.append(bbox)

                # Extract vertices excluding the first six elements (ID and bounding box)
                vertices = components[6:]
                # Check if the number of vertices is even
                if len(vertices) % 2 == 0:
                    # Reshape the vertices into pairs of (x, y) coordinates
                    vertices_pairs = [(vertices[i], vertices[i + 1]) for i in range(0, len(vertices), 2)]
                    # Append the vertices to the list
                    vertices_list.append(vertices_pairs)
                else:
                    print("Error: Odd number of vertices found on line:", line.strip())
            else:
                print("Error: Insufficient data found on line:", line.strip() ,filename)

    return bbox_list, vertices_list






def apply_filter_using_bbox(bbox_data,vertices_data,image,frame_index):
    
    
    fitler_apply_count=1
    
    print("frame index fonksiyon girisi=",frame_index)
    for k in range(len(bbox_data)):
        # Extract bounding box coordinates
        x_min, y_min, x_max, y_max = bbox_data[k]
        
        counter1 = 0
        counter2 = 0
        
        print("frame index start=",frame_index)
        # Iterate through each pixel within the bounding box
        for y in range(y_min, y_max):
            for x in range(x_min, x_max):
                counter1 += 1
                point = (x, y)
                # Ensure vertices_data is a numpy array
                vertices = np.array(vertices_data[k])
                
                if is_point_inside_polygon(point, vertices):
                    # If the point is inside the polygon, apply Gaussian filter to its neighborhood
                    for i in range (fitler_apply_count):
                        apply_gaussian_filter(image, point, window_size=5)
                    counter2 += 1

                
              
        #print("counter1=" + str(counter1)+   " counter2=" +str(counter2)+" bbox="+str(bbox_data[k]))      
        print("frame index end=",frame_index)


    print("frame index fonksiyon cikis=",frame_index)






dataset_number=12
video_path = "dataset_"+str(dataset_number)+"/output_video_"+str(dataset_number)+"_segmented.mp4"


get_all_frames(video_path,dataset_number)







