import cv2
import numpy as np
import os
import re


def create_polygon_mask(image_shape, segment_pairs):
    if not segment_pairs:
        # If segment_pairs are empty, return an empty mask
        return np.zeros(image_shape[:2], dtype=np.uint8)

    # Convert segment_pairs to a 2D numpy array of shape (N, 2)
    segment_pairs = np.array(segment_pairs)
    
    # Reshape segment_pairs into a 2D array
    segment_pairs = np.array(segment_pairs).reshape(-1, 2)

    # Append the first point to the end to ensure closure of the polygon
    segment_pairs = np.vstack([segment_pairs, segment_pairs[0]])

    # Create a blank mask
    mask = np.zeros(image_shape[:2], dtype=np.uint8)
    
    # Create a filled polygon on the mask
    cv2.fillPoly(mask, [segment_pairs], 255)
    
    return mask


def bbox_contain_seg(id_bbox, segment_info):
    margin = 5
    
    x_min, y_min, x_max, y_max = id_bbox
    
    # Expand the bounding box with the margin
    x_min -= margin
    y_min -= margin
    x_max += margin
    y_max += margin
    
    # Iterate over each consecutive pair of points
    for i in range(0, len(segment_info), 2):
        point1 = segment_info[i:i+2]
        if len(point1) != 2:
            # If the pair doesn't contain x and y coordinates, skip it
            continue
        x1, y1 = map(float, point1)
        
        # Check if both points of the pair are outside the expanded bounding box
        if x1 < x_min or x1 > x_max or y1 < y_min or y1 > y_max:
            return False
    
    # If all pairs of points are inside the expanded bounding box
    return True




def write_png(gauss_path,frame_index,dataset_number):
    
   
    
    segmented_frame_path = os.path.join(gauss_path, f"output_video_{dataset_number}_original_{frame_index+1}.png")
    segmented_frame = cv2.imread(segmented_frame_path)
    
    if segmented_frame is None:
       print("frame index="+str(frame_index)+" datatset_number="+str(dataset_number))
       #input()
       return
          
    segmented_frame_with_roi = segmented_frame.copy()
     
    if segmented_frame_with_roi.shape[:2] != original_frame.shape[:2]:
        
        segmented_frame_with_roi =cv2.resize(segmented_frame_with_roi, (original_frame.shape[1], original_frame.shape[0]))
    
    
         
    full_black_image = np.zeros_like(segmented_frame_with_roi)
    output_filename = os.path.join(static_overwrite_mask, f"{frame_index:06d}_full_black_image.png")
    cv2.imwrite(output_filename, full_black_image)
    
    # Save the resulting frame to gauss_path directory, overwriting the original segmented frame
    cv2.imwrite(os.path.join(static_overwrite, f"{frame_index:06d}.png"), segmented_frame_with_roi)
    
    
    print("No masks were created. Skipping frame processing.")





for dataset_number in range (10):
    # Step 1: Process each frame
    
    original_path = f"/home/selim2204/son hal/original/original kıttı {dataset_number}"
    #segmented_path = f"/home/selim2204/son hal/original_yolo/exp{dataset_number}/segmented"
    
    
    gauss_path = f"/home/selim2204/son hal/gaus/kıttı {dataset_number}/gauss result"
    
    static_overwrite= f"/home/selim2204/son hal/static overwrite/kıttı {dataset_number}/static overwrite"
    static_overwrite_mask = f"/home/selim2204/son hal/static overwrite/kıttı {dataset_number}/static overwrite mask"
     
    # Check and create directories if they don't exist
    for directory in [static_overwrite, static_overwrite_mask]:
        if not os.path.exists(directory):
            os.makedirs(directory)
    
    static_txt = f"/home/selim2204/son hal/original_yolo/exp{dataset_number}/object_id_count.txt"
    
    # Dictionary to store object IDs and their counts
    object_counts = {}
    
    # Read the contents of static.txt
    with open(static_txt, 'r') as static_file:
        static_data = static_file.readlines()
    
    # Parse each line to extract object ID and count
    for line in static_data:
        # Split the line by spaces
        parts = line.strip().split()
        
        # Extract the object ID and count
        obj_id = int(parts[3][:-1])  # Extract the number after "Original Object ID:"
        count = int(parts[-1])  # Extract the count
        
        # Store the object ID and count in the dictionary
        object_counts[obj_id] = count
        
        
        
    print(object_counts)
    
    
   
    write_check=0
    for frame_index in range(len(os.listdir(original_path))):
        # Construct the segment points file path for the current frame
        
       
        segment_file_path = f"/home/selim2204/son hal/original_yolo/exp{dataset_number}/left join/output_video_{dataset_number}_original_{frame_index + 1}.txt"
        
        
        
        #segment_file_path = f"/home/selim2204/spyder/dataset_{dataset_number}/left join/output_video_{dataset_number}_original_{frame_index + 1}.txt"
        
        # Check if the segment points file exists
        if not os.path.exists(segment_file_path):
            print(f"Segment points file not found for frame {frame_index}. Skipping frame.")
            write_png(gauss_path,frame_index,dataset_number)
            continue
        
        print(f"Reading segment points from file: {segment_file_path}")
        
        # Read segment points from the file
        segment_points = []
        with open(segment_file_path, "r") as file:
            lines = file.readlines()
            for line in lines:
                # Split the line by spaces
                line_info = line.split()[0:]
                
                obj_id=line_info[0]
                
                
                label = int(line_info[1])
                
              
                # Extract bbox coordinates
                bbox_coords_str = line_info[2:6]
                # Extract numerical values using regular expressions
                bbox_coords = [int(re.search(r'\d+', coord).group()) for coord in bbox_coords_str]
    
                # Extract segment pairs after the eighth element
                segment_pairs = line_info[6:]
                
                
               
    
                # Read segment pairs and convert to integers
                segment_pairs = [(int(segment_pairs[i]), int(segment_pairs[i+1])) for i in range(0, len(segment_pairs), 2)]
                
                # Append line index and segment points to the list
                segment_points.append((obj_id,label, bbox_coords, segment_pairs))
        
        # Ensure segment points are not empty before processing
        if not segment_points:
            print(f"No valid segment points found for frame {frame_index}. Skipping frame.")
            write_png(gauss_path,frame_index,dataset_number)
            continue
        
        print(f"Segment points extracted for frame {frame_index}: {segment_points}")
        
        # Read the current frame
        frame_filename = os.path.join(original_path, f"{frame_index:06d}.png")
        original_frame = cv2.imread(frame_filename)
    
        # Iterate over each segment point
        # Assuming 'segment_points' contains the segment information
        # Initialize a list to store masks
        
        
        anti_mask_segment=[]
        # Iterate over segment points to create masks
        for segment_info in segment_points:
            obj_id, label, bbox_coords, segment_pairs = segment_info
            
            if not segment_pairs:
                print("Segment pairs is empty. Skipping processing.")
                continue
                
            
            
            
            if int(obj_id) in object_counts and  object_counts[int(obj_id)] >0:
                
                
                print(f"Object ID {obj_id} is in static.txt and has a count greater than 5. Proceeding with processing.")
                
                print(segment_pairs)
                
                points = []
                for pair in segment_pairs:
                    points.extend(pair)
                    
                # Create a mask for the polygon defined by segment pairs
                anti_mask = create_polygon_mask(original_frame.shape, points)
                
                # Ensure mask datatype is uint8
                anti_mask = anti_mask.astype(np.uint8)
                
                
                
                    
                
                anti_mask_segment.append(anti_mask)
                
                '''
                if(int(obj_id)==221):
                    cv2.imshow("Mask", anti_mask)
                    cv2.waitKey(0)
                    cv2.destroyAllWindows()
    
                '''
                continue
            else:
                print(f"Object ID {obj_id} is not in static.txt or has a count less than or equal to 5. Skipping processing.")
            
    
           
        
        # Initialize combined_mask as None
        combined_mask = None
        
        # Iterate over each mask in the masks list
        for anti_mask in anti_mask_segment:
            # If combined_mask is None, set it to the current mask
            if combined_mask is None:
                combined_mask = anti_mask
            # Otherwise, combine the current mask with the existing combined_mask using bitwise OR
            else:
                combined_mask = cv2.bitwise_or(combined_mask, anti_mask)
        
       
        segmented_frame_path = os.path.join(gauss_path, f"output_video_{dataset_number}_original_{frame_index+1}.png")
        segmented_frame = cv2.imread(segmented_frame_path)
        
   
        
        
         
        segmented_frame_with_roi = segmented_frame.copy()
         
        if segmented_frame_with_roi.shape[:2] != original_frame.shape[:2]:
            
            segmented_frame_with_roi =cv2.resize(segmented_frame_with_roi, (original_frame.shape[1], original_frame.shape[0]))
        
            
        # Ensure combined_mask is not None
        if combined_mask is not None:
           
            # Check if segmented_frame is None
            if segmented_frame is None:
                print(f"Error reading segmented frame for frame {frame_index}. Skipping frame.")
                input(frame_index)
                
    
            # Overlay the combined mask onto the original frame
            original_frame_with_mask = cv2.bitwise_and(original_frame, original_frame, mask=combined_mask)
            
            # Make a copy of the original frame with the painted mask
            painted_frame = original_frame_with_mask.copy()
            
            # Save the painted frame
            output_filename = os.path.join(static_overwrite_mask, f"{frame_index:06d}_painted_frame.png")
            cv2.imwrite(output_filename, painted_frame)
            
           
            # Debugging code to check dimensions
            print("Combined Mask Shape:", combined_mask.shape)
            print("Segmented Frame Shape:", segmented_frame_with_roi.shape)
            print("Original Frame with Mask Shape:", original_frame_with_mask.shape)
            
                        
           
          
        
            # Check and print the shapes of segmented_frame_with_roi, padded_mask, and original_frame_with_mask
            print("Segmented Frame with ROI Shape:", segmented_frame_with_roi.shape)
            print("Padded Mask Shape:", combined_mask.shape)
            print("Original Frame with Mask Shape:", original_frame_with_mask.shape)
        
           
            segmented_frame_with_roi[combined_mask == 255] = original_frame_with_mask[combined_mask == 255]

            # Save the resulting frame to gauss_path directory, overwriting the original segmented frame
            cv2.imwrite(os.path.join(static_overwrite, f"{frame_index:06d}.png"), segmented_frame_with_roi)
            
            write_check=1
            
         
           
        if write_check == 1:
            write_check=0
        else:
            write_png(gauss_path,frame_index,dataset_number)
            