import numpy as np

dataset_number=0
# Function to convert rotation matrix to quaternion
def rotation_matrix_to_quaternion(rot_matrix):
    trace = np.trace(rot_matrix)
    if trace > 0:
        S = np.sqrt(trace + 1.0) * 2
        qw = 0.25 * S
        qx = (rot_matrix[2, 1] - rot_matrix[1, 2]) / S
        qy = (rot_matrix[0, 2] - rot_matrix[2, 0]) / S
        qz = (rot_matrix[1, 0] - rot_matrix[0, 1]) / S
    elif (rot_matrix[0, 0] > rot_matrix[1, 1]) and (rot_matrix[0, 0] > rot_matrix[2, 2]):
        S = np.sqrt(1.0 + rot_matrix[0, 0] - rot_matrix[1, 1] - rot_matrix[2, 2]) * 2
        qw = (rot_matrix[2, 1] - rot_matrix[1, 2]) / S
        qx = 0.25 * S
        qy = (rot_matrix[0, 1] + rot_matrix[1, 0]) / S
        qz = (rot_matrix[0, 2] + rot_matrix[2, 0]) / S
    elif rot_matrix[1, 1] > rot_matrix[2, 2]:
        S = np.sqrt(1.0 + rot_matrix[1, 1] - rot_matrix[0, 0] - rot_matrix[2, 2]) * 2
        qw = (rot_matrix[0, 2] - rot_matrix[2, 0]) / S
        qx = (rot_matrix[0, 1] + rot_matrix[1, 0]) / S
        qy = 0.25 * S
        qz = (rot_matrix[1, 2] + rot_matrix[2, 1]) / S
    else:
        S = np.sqrt(1.0 + rot_matrix[2, 2] - rot_matrix[0, 0] - rot_matrix[1, 1]) * 2
        qw = (rot_matrix[1, 0] - rot_matrix[0, 1]) / S
        qx = (rot_matrix[0, 2] + rot_matrix[2, 0]) / S
        qy = (rot_matrix[1, 2] + rot_matrix[2, 1]) / S
        qz = 0.25 * S
    return qw, qx, qy, qz
    
# Read the rotation matrices from the file

#file_path="/home/selim2204/quaternion/kıttı 0/orb2 output/orb2_pose_estimation_rot_mat.txt"

file_path = f"03.txt"

rot_matrices = []
with open(file_path, 'r') as file:
    for line in file:
        if line.strip() != '[]':  # Skip lines containing only '[]'
            rot_matrices.append(np.fromstring(line.strip('[]'), sep=' '))


# Read timestamps from the separate file
file_path_timestamps = "times.txt"
timestamps = []
with open(file_path_timestamps, 'r') as file:
    for line in file:
        timestamps.append(float(line.strip()))
        
# Extract X, Y, Z components and quaternions from each row
data = []
for rot_matrix, timestamp in zip(rot_matrices, timestamps):
    rot_matrix = rot_matrix.reshape(3, 4)
    
    # Debug: Print timestamp
    print("Timestamp:", timestamp)
    
    qw, qx, qy, qz = rotation_matrix_to_quaternion(rot_matrix)
    xyz = rot_matrix[:, 3]
    
    # Debug: Print XYZ components and quaternion components
    print("XYZ:", xyz)
    print("Quaternion components:", qw, qx, qy, qz)
    
    # Append timestamp followed by X, Y, Z components of the rotation matrix and quaternion components
    data.append([timestamp] + xyz.tolist() + [qw, qx, qy, qz])
    
    

# File path to save the data
#output_file_path = "/home/selim2204/quaternion/kıttı 0/orb2 output/orb2_estimation_quat.txt"

output_file_path = "03_ground_quat.txt"

with open(output_file_path, 'w') as output_file:
    # Write data to the file
    for row in data:
       
        if str(row) == '[]':
            output_file.write('[]\n')
        else:
            formatted_row = ["{:.16f}".format(x) for x in row]
            output_file.write(' '.join(formatted_row) + '\n')
            
print("Data saved to:", output_file_path)
