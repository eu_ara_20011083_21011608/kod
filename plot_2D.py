import numpy as np
import matplotlib.pyplot as plt
import os

def plot_and_save_projections(directory_path):
    # Iterate through all subdirectories
    for subdir in os.listdir(directory_path):
        subdir_path = os.path.join(directory_path, subdir)
        if os.path.isdir(subdir_path):
            file_path = os.path.join(subdir_path, "pose_information.txt")

            if os.path.isfile(file_path):
                try:
                    # Load data from the pose_information.txt file, only columns 1 to 3 (x, y, z)
                    data = np.loadtxt(file_path, usecols=(1, 2, 3), skiprows=0, max_rows=1000)  

                    # Extract x, y, z coordinates
                    x = data[:, 0]
                    y = data[:, 1]
                    z = data[:, 2]

                    # Scale y values
                    y = y * 100  # Scale y values by 100

                    # Calculate partial sums
                    partial_sum_x = np.cumsum(x)
                    partial_sum_y = np.cumsum(y)
                    partial_sum_z = np.cumsum(-z)

                    # Set figure size
                    fig, axs = plt.subplots(3, 1, figsize=(10, 24))  # 3 rows, 1 column

                    # Plot xz plane
                    scatter_xz = axs[0].scatter(partial_sum_x, partial_sum_z, c=np.arange(len(x)), cmap='viridis', marker='o', s=10)
                    axs[0].set_xlabel('X')
                    axs[0].set_ylabel('Z')
                    axs[0].set_title('XZ Projection')
                    cbar = plt.colorbar(scatter_xz, ax=axs[0])
                    cbar.set_label('Index')
                    axs[0].set_aspect('equal', adjustable='box')

                    # Plot xy plane
                    scatter_xy = axs[1].scatter(partial_sum_x, partial_sum_y, c=np.arange(len(x)), cmap='viridis', marker='o', s=10)
                    axs[1].set_xlabel('X')
                    axs[1].set_ylabel('Y (scaled)')
                    axs[1].set_title('XY Projection(Y values scaled by 100)')
                    cbar = plt.colorbar(scatter_xy, ax=axs[1])
                    cbar.set_label('Index')
                    axs[1].set_aspect('equal', adjustable='box')

                    # Plot yz plane
                    scatter_yz = axs[2].scatter(partial_sum_y, partial_sum_z, c=np.arange(len(x)), cmap='viridis', marker='o', s=10)
                    axs[2].set_xlabel('Y (scaled)')
                    axs[2].set_ylabel('Z')
                    axs[2].set_title('YZ Projection(Y values scaled by 100)')
                    cbar = plt.colorbar(scatter_yz, ax=axs[2])
                    cbar.set_label('Index')
                    axs[2].set_aspect('equal', adjustable='box')

                    plt.tight_layout()

                    # Save the figure in the same subdirectory
                    output_path = os.path.join(subdir_path, f"{subdir}_projections.png")
                    plt.savefig(output_path)
                    plt.close(fig)

                except ValueError as e:
                    print(f"Error processing file {file_path}: {e}")

# Define the main directory path containing all subdirectories
main_directory_path = "orb_outputs"  # Update with your main directory path

# Plot and save projections for each subdirectory
plot_and_save_projections(main_directory_path)

