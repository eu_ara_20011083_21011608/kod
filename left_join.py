import os

# Function to read the content of a text file and store it in a list of dictionaries
def read_txt_file(file_path, is_id_file=True):
    data = []
    with open(file_path, 'r') as file:
        for line in file:
            parts = line.strip().split()
            if is_id_file:
                object_id = parts[0]
                class_name = parts[1]
                bbox = tuple(map(float, parts[2:]))  # Read bbox info from index 1
                data.append((object_id, class_name, bbox))
            else:
                class_name = parts[0]
                bbox = tuple(map(float, parts[1:5]))  # Read bbox info from index 1
                segment_info=parts[6:]
                data.append((class_name, bbox,segment_info))
    return data


def bbox_contain_seg(id_bbox, segment_info):
    margin = 20
    
    x_min, y_min, x_max, y_max = id_bbox
    
    # Expand the bounding box with the margin
    x_min -= margin
    y_min -= margin
    x_max += margin
    y_max += margin
    
    # Iterate over each consecutive pair of points
    for i in range(0, len(segment_info), 2):
        point1 = segment_info[i:i+2]
        if len(point1) != 2:
            # If the pair doesn't contain x and y coordinates, skip it
            continue
        x1, y1 = map(float, point1)
        
        # Check if both points of the pair are outside the expanded bounding box
        if x1 < x_min or x1 > x_max or y1 < y_min or y1 > y_max:
            return False
    
    # If all pairs of points are inside the expanded bounding box
    return True

    


def write_txt_file(file_path, data):
    with open(file_path, 'a') as file:
        for item in data:
            formatted_item = ' '.join(map(str, item)).replace("[", "").replace("]", "").replace("'", "").replace(",", "").replace("(", "").replace(")", "")
            file.write(formatted_item + '\n')


for dataset_number in range(10):
    
    '''
    # Directory paths
    id_dir = f'/home/selim2204/spyder/dataset_{dataset_number}/bbox save'
    segment_dir = '/home/selim2204/spyder/dataset_{dataset_number}/segment save'
    output_dir = '/home/selim2204/spyder/dataset_{dataset_number}/left join'
    '''
    
    
    id_dir = f'/home/selim2204/son hal/original_yolo/exp{dataset_number}/bbox save'
    segment_dir = f'/home/selim2204/son hal/original_yolo/exp{dataset_number}/segment save'
    output_dir = f'/home/selim2204/son hal/original_yolo/exp{dataset_number}/left join'
   
        # Check if the directory exists
    if not os.path.exists(output_dir):
        # If it doesn't exist, create it
        os.makedirs(output_dir)
        
    # Clear files from the output directory
    for filename in os.listdir(output_dir):
        file_path = os.path.join(output_dir, filename)
        if os.path.isfile(file_path):
            os.remove(file_path)
    
    
    # Iterate over files in id_dir
    for id_filename in os.listdir(id_dir):
        if id_filename.endswith(".txt"):
            print("Processing ID file:", id_filename)
            # Read data from id file
            id_file_path = os.path.join(id_dir, id_filename)
            id_data = read_txt_file(id_file_path, is_id_file=True)
            
            # Get the index from the ID filename
            id_index = id_filename.split("_")[-1].split(".")[0]
            
            
            # Construct the expected segment filename
            segment_filename = f"output_video_{dataset_number}_original_{id_index}.txt"
            segment_file_path = os.path.join(segment_dir, segment_filename)
            
            # Iterate over each row in id data
            for id_row in id_data:
                id_object_id, id_class_name, id_bbox = id_row
                print("ID Object ID:", id_object_id)
                print("ID Class Name:", id_class_name)
                print("ID Bounding Box:", id_bbox)
                
                # Check if the corresponding segment file exists
                if os.path.exists(segment_file_path):
                    print("Processing segment file:", segment_filename)
                    
                    # Read data from segment file
                    segment_data = read_txt_file(segment_file_path, is_id_file=False)
                    
                    # Iterate over each row in segment data
                    for segment_row in segment_data:
                        segment_class_name, segment_bbox, segment_info = segment_row
                        print("Segment Class Name:", segment_class_name)
                        print("Segment Bounding Box:", tuple(map(int, segment_bbox)))
                        print([int(coord) for coord in segment_info])
                        # Compare if values are approximately the same
                        if bbox_contain_seg(id_bbox, segment_info):
                            print("Bounding boxes ARE approximately the same.-------------------------")
                            # Create a text file in the output directory and append the row
                            output_file_path = os.path.join(output_dir, f"{id_filename.split('.')[0]}.txt")
                            write_txt_file(output_file_path, [(id_object_id, segment_class_name, tuple(map(int, segment_bbox)), [int(coord) for coord in segment_info])])
                        else:
                            print("Bounding boxes are not approximately the same.")
    
                else:
                    print("Segment file not found for index:", id_index)