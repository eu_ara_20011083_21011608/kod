import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics.pairwise import cosine_similarity
import seaborn as sns  # Import seaborn for heatmap visualization

similarity_save = []


# Function to read data from a text file
def read_data(file_path):
    with open(file_path, 'r') as file:
        data = np.loadtxt(file)
    return data

# Function to find the index of the nearest value in an array
def find_nearest(array, value):
    idx = (np.abs(array - value)).argmin()
    return idx


def quaternion_inverse(q):
    
    w, x, y, z = q  # Unpack the first row if it's a 2D array
    norm_squared = w**2 + x**2 + y**2 + z**2
    
    return (w/norm_squared, -x/norm_squared, -y/norm_squared  ,-z/norm_squared)
   



def quaternion_multiply(q1, q2):
    w1, x1, y1, z1 = q1
    w2, x2, y2, z2 = q2
    
   
    w = w1*w2 - x1*x2 - y1*y2 - z1*z2
    x = w1*x2 + x1*w2 + y1*z2 - z1*y2
    y = w1*y2 - x1*z2 + y1*w2 + z1*x2
    z = w1*z2 + x1*y2 - y1*x2 + z1*w2
    
  
    return (w, x, y, z)


def rotation_quaternion(v1, v2):
    # Normalize the input vectors
    v1 = v1 / np.linalg.norm(v1)
    v2 = v2 / np.linalg.norm(v2)

    
    # Calculate the axis of rotation
    axis = np.cross(v1, v2)
 
    axis /= np.linalg.norm(axis)

    # Calculate the angle of rotation
    angle = np.arccos(np.dot(v1, v2))
    print("angle=",angle*360/(2*np.pi),"-------------------------------------")
    # Calculate the quaternion components
    w = np.cos(angle / 2)
    x = axis[0] * np.sin(angle / 2)
    y = axis[1] * np.sin(angle / 2)
    z = axis[2] * np.sin(angle / 2)

    return np.array([w, x, y, z])


def quaternion_rotation_apply(vector, quaternion):
    
    q_inverse = quaternion_inverse(quaternion)
   
    # Convert vector to quaternion representation
    vector_quaternion_form = np.array([0, vector[0], vector[1], vector[2]])

    # Perform quaternion multiplication
    rotated_vector_quaternion = np.zeros(4)
   
    
    rotated_vector_quaternion = quaternion_multiply(quaternion,vector_quaternion_form)
    result = quaternion_multiply(rotated_vector_quaternion,q_inverse)
 
    '''
    rotated_vector_quaternion = quaternion_multiply(q_inverse,vector_quaternion_form)
    result = quaternion_multiply(rotated_vector_quaternion,quaternion)
    '''
    '''
    rotated_vector_quaternion = quaternion_multiply(vector_quaternion_form,quaternion)
    result = quaternion_multiply(q_inverse,rotated_vector_quaternion)
    '''
    # Convert back to vector representation
    rotated_vector = result[1:]

    return rotated_vector




def rotate_chunk(chunk_arr1, quaternion):
    rotated_chunk_arr1 = []

    for vector in chunk_arr1:
        
        rotated_vector = quaternion_rotation_apply(vector, quaternion)
        
        print("vector=",vector)
        print("rotated_vector=",rotated_vector)
        rotated_chunk_arr1.append(rotated_vector)

    return np.array(rotated_chunk_arr1)


def calculate_cosine_similarity(chunk_arr1, chunk_arr2,save_set):
    similarities = []
    for vec1, vec2 in zip(chunk_arr1, chunk_arr2):
        similarity = cosine_similarity([vec1], [vec2])[0, 0]
        '''
        if similarity < 0.00001:
            print("vec1=",vec1)
            print("vec2=",vec2)
            input(save_set)
        '''     
        similarities.append(similarity)
       
        if(save_set==1):
            similarity_save.append(similarity)
            
    return similarities


def plot_with_cosine_similarity(chunk_arr1, chunk_arr2,başarı_threshold,save_set):
    similarities = calculate_cosine_similarity(chunk_arr1, chunk_arr2,save_set)
    
    above_threshold = np.sum(np.array(similarities) > başarı_threshold)
  
    plt.figure(figsize=(8, 6))
    plt.scatter(range(len(similarities)), similarities, marker='o', color='blue')
    plt.xlabel('Pair Index')
    plt.ylabel('Cosine Similarity')
    plt.title('Cosine Similarity Plot')
    plt.grid(True)
    plt.ylim(-1, 1)  # Set y-axis limit to [-1, 1]
    plt.show()
    
    return above_threshold

    

def calculate_mean_chunks(arr1, arr2, chunk_size,scale_constant=100):
    
    başarı=0
    başarı_threshold = 0.5
   
    for i in range(0, len(arr1), chunk_size):
        chunk_arr1 = arr1[i:i+chunk_size]
        chunk_arr2 = arr2[i:i+chunk_size]
        
        
        chunk_arr1=chunk_arr1*scale_constant
        
        partial_sums1 = np.cumsum(chunk_arr1, axis=0)
        partial_sums2 = np.cumsum(chunk_arr2, axis=0)

       
        plt.scatter(partial_sums1[:, 0], partial_sums1[:, 2], c=range(len(partial_sums1)), cmap='viridis', marker='^', label='partial_sums1 before')
        plt.scatter(partial_sums2[:, 0], partial_sums2[:, 2], c=range(len(partial_sums2)), cmap='viridis', marker='o', label='partial_sums2 before')
       
        plt.colorbar(label='Index')  # Add colorbar to show the index-color mapping
        plt.xlabel('X Label')
        plt.ylabel('Z Label')
        
        plt.legend()
        plt.show()
        
        plot_with_cosine_similarity(chunk_arr1, chunk_arr2,başarı_threshold,0)
        
        
        
        
        
        midpoint = len(chunk_arr1) // 2

        first_half_arr1 = partial_sums1[:midpoint]
        second_half_arr1 = partial_sums1[midpoint:]

        first_half_arr2 = partial_sums2[:midpoint]
        second_half_arr2 = partial_sums2[midpoint:]

       
        mean_point_first_half_arr1 = np.mean(first_half_arr1, axis=0)
        mean_point_second_half_arr1 = np.mean(second_half_arr1, axis=0)
        
        mean_point_first_half_arr2 = np.mean(first_half_arr2, axis=0)
        mean_point_second_half_arr2 = np.mean(second_half_arr2, axis=0)
        
        
        v1 = mean_point_second_half_arr1 - mean_point_first_half_arr1
        v2 = mean_point_second_half_arr2 - mean_point_first_half_arr2
        print("v1=",v1,)
        print("v1=",v2)
        
        
        rot_q=rotation_quaternion(v1, v2)
        print("rot_q=",rot_q)
        rotated_chunk_arr1 = rotate_chunk(chunk_arr1,rot_q)
        
       
        
        rotated_partial_sums1 = np.cumsum(rotated_chunk_arr1, axis=0)

        
 
        
        plt.scatter(rotated_partial_sums1[:, 0], rotated_partial_sums1[:, 2], c=range(len(rotated_partial_sums1)), cmap='viridis', marker='^', label='rotated_partial_sums1')
        plt.scatter(partial_sums2[:, 0], partial_sums2[:, 2], c=range(len(partial_sums2)), cmap='viridis', marker='o', label='partial_sums2')
       
        plt.xlabel('X Label')
        plt.ylabel('Z Label')
        
        plt.colorbar(label='Index')  # Add colorbar to show the index-color mapping
        plt.legend()
        plt.show()

        başarı += plot_with_cosine_similarity(rotated_chunk_arr1, chunk_arr2,başarı_threshold,1)
    
    
        #input() 
    return başarı









file1_data = read_data('/home/selim2204/quaternion/çöp/aligned_pose_information.txt')



file1_data = read_data('/home/selim2204/quaternion/kıttı 0/KeyFrameTrajectory_orb3.txt')

file1_data = read_data('/home/selim2204/quaternion/kıttı 0/estimated_quat.txt')

file1_data = read_data('/home/selim2204/quaternion/kıttı 1/01_original/pose_information.txt')






file2_data = read_data('/home/selim2204/quaternion/kıttı 0/00_ground_quat.txt')


file2_data = read_data('/home/selim2204/quaternion/kıttı 1/01_ground_quat.txt')


# Extract timestamps from the first column of the first file
timestamps_file1 = file1_data[:, 0]




# Initialize variables to store results
change = []
change_gt = []
'''
cross = []
cross_gt = []
'''






prev_values_file1=-99999#hata çıkarsa anlamak adına büyük bir sayı verdim
prev_values_file2=-99999


# Loop through each timestamp in the first file
for i, timestamp in enumerate(timestamps_file1):
    # Find the index of the nearest timestamp in the second file
    nearest_idx = find_nearest(file2_data[:, 0], timestamp)

    # Get corresponding values from file 1
    values_file1 = file1_data[i, 1:4]
   
   
    
    # Calculate differences with the previous entry
   
    change.append(values_file1),
        
 
    if i > 0:
        diff2 = file2_data[nearest_idx, 1:4] - prev_values_file2
        
        change_gt.append(diff2)


    
    # Print corresponding values from both files
    print(f"Timestamp from File 1: {timestamp:.4f} == {file2_data[nearest_idx, 0]:.4f}")
    print("F1", np.around(values_file1, 4))
    print("GT", ' '.join(f"{val:.4g}" for val in file2_data[nearest_idx, 1:4]))
    print()

  
     
    # Save current values as previous for the next iteration
    prev_values_file1 = values_file1
    prev_values_file2 = file2_data[nearest_idx, 1:4]
    
    
# Convert change to numpy array
change = np.array(change)
change_gt = np.array(change_gt)




change[:,1]=0
change_gt[:,1]=0


change[:,0]=change[:,0]*+1

change[:,1]=change[:,1]*+1

 
change[:,2]=change[:,2]*-1
 

'''
tmp=change[:,0]
change[:,0]=change[:,2]
change[:,,2]=tmp
'''


# Example usage:
chunk_size = 200  # Set your desired chunk size
scale_constant = 0.5

başarı = calculate_mean_chunks(change, change_gt, chunk_size,scale_constant)

num_elements=change.shape[0]

print("başarı=",başarı)
print("Number of elements in change array:", num_elements)


plt.figure(figsize=(8, 6))
plt.scatter(range(len(similarity_save)), similarity_save, marker='o', color='blue')
plt.xlabel('Index')
plt.ylabel('Cosine Similarity')
plt.title(f'başarı={başarı} total={num_elements}')
plt.grid(True)
plt.ylim(-1, 1)  # Set y-axis limit to [-1, 1]
plt.show()