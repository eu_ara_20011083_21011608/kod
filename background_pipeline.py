import os
import shutil
import subprocess
import time

# Define paths

frame_number = 3

for frame_number in range(10):
    # Construct the frame input directory path
    frame_input_dir = f"/home/selim2204/DeepMCBM/input/original kıttı {frame_number}"
    
    frame_output_dir = "/home/selim2204/DeepMCBM/input/input_frames/frames"
    background_estimation_dir = "/home/selim2204/DeepMCBM/output/input_frames/my_run/background_estimation"
    
    destination_path = f"/home/selim2204/son hal/background/kıttı {frame_number}"
    
    
    
    # Create the frame output directory if it doesn't exist
    os.makedirs(frame_output_dir, exist_ok=True)
    
    # Initialize counter for frame names
    frame_counter = 0
    chunk=12
    
    # Get list of frame files
    frame_files = sorted(os.listdir(frame_input_dir))
    
    # Process frames in groups of 5
    for i in range(0, len(frame_files), chunk):
        # Clean the frame output directory
        for file in os.listdir(frame_output_dir):
            file_path = os.path.join(frame_output_dir, file)
            try:
                if os.path.isfile(file_path):
                    os.unlink(file_path)
            except Exception as e:
                print(e)
        
        # Move 5 frames to DeepMCBM input directory
        for j in range(chunk):
            idx = i + j
            if idx < len(frame_files):
                src_file = os.path.join(frame_input_dir, frame_files[idx])
                dst_file = os.path.join(frame_output_dir, f"{frame_counter:06d}.png")
                shutil.copy(src_file, dst_file)
                print(f"Added frame {frame_files[idx]} as {dst_file}")
                frame_counter += 1
    
        # Run DeepMCBM script
        print("Running DeepMCBM script...")
        subprocess.run(["python", "/home/selim2204/DeepMCBM/src/DeepMCBM.py", "--no_train_BMN", "--no_train_STN"])
    
        # Wait for the expected files to be delivered
        timeout = 60  # Adjust the timeout period as needed
        start_time = time.time()
    
        while not os.path.exists(background_estimation_dir) or len([file for file in os.listdir(background_estimation_dir) if file.startswith("frame") and file.endswith(".png")]) < 5:
            if time.time() - start_time > timeout:
                print("Timeout reached. Expected files were not delivered.")
                break
            time.sleep(1)  # Wait for 1 second before checking again
    
        
        file_name_counter=chunk
       # Rename the output frames in background_estimation_dir
        for new_frame_index, file in enumerate(sorted(os.listdir(background_estimation_dir))):
            if file.startswith("frame") and file.endswith(".png"):
                old_file_path = os.path.join(background_estimation_dir, file)
                
                
                print("tmp_frame_counter="+str(frame_counter)+" file_name_counter="+str(file_name_counter))
                
                new_file_path = os.path.join(destination_path, f"Frame{frame_counter - file_name_counter:06d}.png")
                file_name_counter-=1
                
                print(f"Renaming file from {file} to {new_file_path}")
                os.rename(old_file_path, new_file_path)
    
        #input()
