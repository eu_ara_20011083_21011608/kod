% Read positions and quaternions from the file



file_path2 = 'matlab3d/combined_data_file2.txt'
pose_set=0




file_path = '01_ground_quat.txt';
pose_set=0

file_path = 'dataset\01\01\01_original/pose_information.txt'
pose_set=1

file_path1 = 'matlab3d/combined_data_file1.txt'
pose_set=0



file_path2 = 'matlab3d_2/combined_data_file2.txt'
pose_set=0

file_path1 = 'matlab3d_2/combined_data_file1.txt'
pose_set=0




scale=200
[normal_save2, x_save, y_save, z_save]=main(file_path2,scale,pose_set,0,0,0,0)


scale=200
use_save_xyz=1
normal_save1 = main(file_path1, scale, pose_set,use_save_xyz,x_save, y_save, z_save);


% Initialize array to store cosine similarities
cosine_similarities = zeros(length(normal_save1), 1);

% Iterate through each orientation axis and compute cosine similarity
for i = 1:length(normal_save1)
    % Extract orientation axes from both saves
    axis1 = normal_save1{i};
    axis2 = normal_save2{i};
    
    % Compute cosine similarity
    cosine_similarities(i) = dot(axis1, axis2) / (norm(axis1) * norm(axis2));
end


% Define the threshold
threshold = 0.5;

% Count the number of values bigger than the threshold in absolute value
num_values_above_threshold = sum(abs(cosine_similarities) > threshold);

% Total number of elements
total_elements = numel(cosine_similarities);


% Plot 2D cosine similarities as dots
figure;
scatter(1:length(cosine_similarities), cosine_similarities);
xlabel('Index');
ylabel('Cosine Similarity');
title('Cosine Similarity Between Orientation Axes');
% Add annotation for the number of values above the threshold
text(0.5, 0.9, sprintf('Values above threshold: %d', num_values_above_threshold), 'Units', 'normalized', 'HorizontalAlignment', 'center');

% Add annotation for the total number of elements
text(0.5, 0.8, sprintf('Total elements: %d', total_elements), 'Units', 'normalized', 'HorizontalAlignment', 'center');



function [orientation_axes_save, x_coords, y_coords, z_coords] = main(file_path, scale, pose_set,use_save_xyz,x_save, y_save, z_save)
   
    [timestamps, positions, quaternions] = read_positions_and_quaternions(file_path);
    
    
    % Find rows where timestamp is less than or equal to 300
    valid_indices = (timestamps >= 0) & (timestamps <= 100);

    
    % Filter out positions, quaternions, and timestamps based on valid indices
    timestamps = timestamps(valid_indices);
    positions = positions(valid_indices, :);
    quaternions = quaternions(valid_indices, :);
    
    if use_save_xyz==1
        x_coords = x_save
        y_coords = y_save
        z_coords = z_save
    else   
        % Extract X, Y, Z coordinates
        x_coords = positions(:, 1);
        y_coords = positions(:, 2);
        z_coords = positions(:, 3);
    end

    % Plot the 3D trajectory
    figure;
    plot3(x_coords, y_coords, z_coords, 'b-o');
    xlabel('X Coordinate');
    ylabel('Y Coordinate');
    zlabel('Z Coordinate');
    title('3D Map of Pose Information with Orientations');
    text(0.02, 0.95, 1, ['File Path: ' file_path], 'Units', 'normalized', 'Color', 'blue');
    grid on;
    hold on;
    
    % Plot the orientation using quivers
  
    
    
    % Initialize cell array to store orientation axes
    orientation_axes_save = cell(1, length(timestamps));
    counter = 0;  % Initialize counter

    for i = 1:length(timestamps)
        quat = quaternions(i, :);
        
        
        % Normalize quaternion to ensure unit quaternion
        quat = quat/norm(quat);
        
        if(pose_set==1)
           theta = 2*acos(quat(4));
           orientation_axis(1)=quat(1)/sin(theta/2)
           orientation_axis(2)=quat(2)/sin(theta/2)
           orientation_axis(3)=quat(3)/sin(theta/2)
    
        else
            theta = 2*acos(quat(1));
            orientation_axis(1)=quat(2)/sin(theta/2)
            orientation_axis(2)=quat(3)/sin(theta/2)
            orientation_axis(3)=quat(4)/sin(theta/2)
        end
        
        orientation_axes_save{i} = orientation_axis;
        counter = counter + 1;  % Increment counter



        saved_x_coords(i) = x_coords(i)
        saved_y_coords(i) = y_coords(i)
        saved_z_coords(i) = z_coords(i)
    

    
        % Scale the orientation axis for visualization
        orientation_axis = scale * orientation_axis;
        

        % Plot the orientation arrow
        quiver3(x_coords(i), y_coords(i), z_coords(i), ...
                orientation_axis(1), orientation_axis(2), orientation_axis(3), ...
                'r', 'LineWidth', 1);
    end
    
    disp(['Number of elements added to orientation_axes_save: ', num2str(counter)]);
    disp(['length(timestamps): ', num2str( length(timestamps))]);   
    %pause(10);
    
    hold off;
    
    % Animate the trajectory
    figure;
    h = plot3(x_coords(1), y_coords(1), z_coords(1), 'b-o');
    xlabel('X Coordinate');
    ylabel('Y Coordinate');
    zlabel('Z Coordinate');
    title('3D Map of Pose Information with Orientations');
    grid on;
    hold on;
 

end

function [timestamps, positions, quaternions] = read_positions_and_quaternions(file_path)
    data = readmatrix(file_path);
    timestamps = data(:, 1);
    positions = data(:, 2:4);
    quaternions = data(:, 5:8);
end